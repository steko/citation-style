A [CSL citation style](http://citationstyles.org/) for the _Archeologia in Liguria_ journal (ISSN 2499-927X).

Based on the [Anglia Ruskin University - Harvard](https://www.zotero.org/styles/harvard-anglia-ruskin-university) style by Steven Singleton.